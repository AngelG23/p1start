package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{

	@Override
	public int compare(Car o1, Car o2) {
		// TODO Auto-generated method stub
		String temp1 = o1.getCarBrand().concat(o1.getCarModel()).concat(o1.getCarModelOption());
		String temp2 = o2.getCarBrand().concat(o2.getCarModel()).concat(o2.getCarModelOption());
		return temp1.compareToIgnoreCase(temp2);
	}
	
	

}
