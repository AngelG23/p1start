package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarList {
	private static HashTableOA<Integer, Car> cList;
	private static CarList singleton = new CarList();
	
	private CarList() {
		cList = new HashTableOA<Integer,Car>();
		}
	
	public static HashTableOA<Integer, Car> getInstance(){
		return cList;
	}
	public static void resetCars() {
		 
			cList =	new HashTableOA<Integer, Car>();
	}
	
}
