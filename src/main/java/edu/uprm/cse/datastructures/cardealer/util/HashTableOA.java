package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class HashTableOA<K, V> implements Map<K, V> {
	
	public static class MapEntry<K,V> {
		private K key;
		private V value;
		
		//This boolean will be used to set the state of the MapEntry.
		//If it it true, the Map Entry was deleted. 
		//If it is false, the MapEntry was not deleted
		private boolean deleted;
		
		public boolean isDeleted() {
			return deleted;
		}
		public void setDeleted(boolean deleted) {
			this.deleted = deleted;
		}
		public K getKey() {
			return key;
		}
		public void setKey(K key) {
			this.key = key;
		}
		public V getValue() {
			return value;
		}
		public void setValue(V value) {
			this.value = value;
		}
		public MapEntry(K key, V value, boolean deleted) {
			super();
			this.key = key;
			this.value = value;
			this.deleted = deleted;
		}
		
		
		
	}

	private int currentSize;
	private  Object[] buckets;
	
	
	private static final int DEFAULT_BUCKETS = 11;
	
	private int FirstHashFunction(K key) {
		return key.hashCode()*2 % this.buckets.length;
	}
	
	private int SecondHashFunction(K key) {
		return (FirstHashFunction(key) * FirstHashFunction(key)) % this.buckets.length;
	}
	
	public HashTableOA(int numBuckets) {
		this.currentSize  = 0;
		this.buckets = new Object[numBuckets];
		
		for (int i =0; i < numBuckets; ++i) {
			this.buckets[i] = new MapEntry<K, V>(null, null, false);
		
		}
	}
	
	public HashTableOA() {
		this(DEFAULT_BUCKETS);
	}
	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public V get(K key) {
		int firstHash = this.FirstHashFunction(key);
		int secondHash = this.SecondHashFunction(key);
		MapEntry<K, V> temp = (MapEntry<K, V>) this.buckets[firstHash];
		while(firstHash != secondHash || (temp != null && !temp.isDeleted()) ) {
			if(temp.getKey().equals(key)) {
				return temp.getValue();
			}
			else {
				temp = (MapEntry<K, V>) this.buckets[secondHash];
				secondHash++;
			}
		}
		return null;
//		int targetBucket = this.FirstHashFunction(key);
//		MapEntry<K, V> returnME = 
//				 (MapEntry<K, V>) this.buckets[targetBucket];
//		boolean found = false;
//		int temp = this.SecondHashFunction(key);
//		while(!found) {
//			
//			if (returnME.getKey().equals(key)) {
//
//				found = true;
//				return returnME.getValue();
//			}	
//			
//			
//			if(temp == targetBucket || (returnME == null && returnME.getState() == false)) {
//				return null;
//			}else {
//				returnME = (MapEntry<K, V>) this.buckets[temp];
//				temp = (temp + 1) % this.size();
//			}
//			
//			}
//		return null;
		}

	@Override
	public V put(K key, V value) {
		
		MapEntry<K, V> temp = new MapEntry<K, V>(key, value, false);
		int firstHash = this.FirstHashFunction(temp.getKey());
		int secondHash = this.SecondHashFunction(temp.getKey());
		boolean found = false;
		if(this.currentSize == this.size()) {
			reAllocate();
		}
		while(!found) {
			MapEntry<K, V> bucketTemp = (MapEntry<K, V>) this.buckets[firstHash];
			if(bucketTemp.getKey().equals(null)) {
				this.buckets[firstHash] = temp;
				found = true;
				this.currentSize++;
				return temp.getValue();
			}else {
				MapEntry<K, V> bucketTemp2 = (MapEntry<K, V>) this.buckets[secondHash];
				if(bucketTemp2.equals(null)) {
					this.buckets[secondHash] = temp;
					found = true;
					this.currentSize++;
					return temp.getValue();
				}
				else if(secondHash == firstHash)return null;
				else{
					secondHash++;
				}
				
			}
		}
		return null;
//		V oldValue = this.get(key);
//		if (oldValue != null) {
//			this.remove(key);
//		}
//		int targetBucket = this.FirstHashFunction(key);
//		MapEntry<K,V> L = (MapEntry<K,V>) this.buckets[targetBucket];
//		MapEntry<K,V> M = new MapEntry<K,V>(key, value);
//		L.add(M, 0);
//		this.currentSize++;
//
//		return oldValue;
	}

	@Override
	public V remove(K key) {
		
		int firstHash = this.FirstHashFunction(key);
		int secondHash = this.SecondHashFunction(key);
		int index = firstHash;
		MapEntry<K, V> temp = (MapEntry<K, V>) this.buckets[firstHash];
		while(index != secondHash || (temp != null && temp.isDeleted())) {
			if(temp.getKey().equals(key)) {
				V result = temp.getValue();
				temp.setDeleted(true);
				temp.setKey(null);
				this.buckets[index] = temp;
				return result;
			}
			else {
				temp = (MapEntry<K, V>) this.buckets[secondHash];
				secondHash = index++;
				
			}
		}
		return null;
//		int targetBucket = this.FirstHashFunction(key);
//		MapEntry<K,V> removeEntry = (MapEntry<K,V>) this.buckets[targetBucket];
//		int secondHash = this.SecondHashFunction(key);
//		boolean found = false;
//		while(!found) {
//			if(removeEntry.getKey().equals(key)) {
//				found = true;
//				V result = removeEntry.getValue();
//				this.buckets[targetBucket] = null;
//				this.currentSize--;
//				return result;
//			}
//			else {
//				if(secondHash == targetBucket) {
//					return null;
//				}
//				else {
//					removeEntry = (MapEntry<K, V>) this.buckets[secondHash];
//					targetBucket = secondHash;
//					secondHash++;
//				}
//			}
//		}
//		return null;
		
//		for (MapEntry<K,V> M: L ) {
//			if (M.getKey().equals(key)) {
//				// borrar
//				V result = M.getValue();
//				L.remove(i);
//				this.currentSize--;
//				return result;
//			}
//			else {
//				i++;
//			}
//		}
//		return null;
	}

	@Override
	public boolean contains(K key) {
		return this.get(key) != null;
	}

	@Override
	public SortedList<K> getKeys() {
		CircularSortedDoublyLinkedList<K> result = new CircularSortedDoublyLinkedList<K>();
		
		for (Object o : this.buckets) {
			MapEntry<K,V> L = (MapEntry<K,V>) o;
				result.add(L.getKey());
		}
		return result;
	}

	@Override
	public SortedList<V> getValues() {
		SortedList<V> result = new CircularSortedDoublyLinkedList<V>();
		
		for (Object o : this.buckets) {
			MapEntry<K,V> L = (MapEntry<K,V>) o;
				result.add(L.getValue());
			
		}
		return result;
	}

	
	public void reAllocate() {
		// TODO Auto-generated method stub
		
		Object[] temp = this.buckets;
		this.buckets = new Object[this.buckets.length*2];
		this.currentSize = 0;
		for(int i = 0; i < temp.length-1; i++) {
			MapEntry<K, V> stationary = (MapEntry<K, V>) temp[i];
			this.put(stationary.getKey(), stationary.getValue());
		}
		
//			HashTableOA temp = new HashTableOA();
//			
//			
//			
//			for(Object M: this.buckets) {
//				MapEntry<K, V> temp2 = (MapEntry<K, V>) M;
//				temp.put(temp2.getKey(), temp2.getValue());
//				
//				
//				
//			}
//			return temp;
		
		
	}

}
