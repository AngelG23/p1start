package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.util.HashTableOA.MapEntry;

public class DeletedEntry extends MapEntry {

    private static DeletedEntry entry = null;



    private DeletedEntry() {

          super(-1, -1);

    }



    public static DeletedEntry getUniqueDeletedEntry() {

          if (entry == null)

                entry = new DeletedEntry();

          return entry;

    }

}
