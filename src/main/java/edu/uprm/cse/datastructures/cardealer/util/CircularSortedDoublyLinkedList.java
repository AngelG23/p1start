package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;



public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	
	//Circular 
	@SuppressWarnings("hiding")
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;
		private Node<E> prevNode;
		int index = -1;
		
		
		@SuppressWarnings("unchecked")
		public void CircularSortedDoubLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}
		@Override
		public boolean hasNext() {
			return nextNode != null;
		}
		
		
		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}

	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> previous;
		
		public Node(E element, Node<E> next, Node<E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.previous = prev;
		}
		public Node(E element) {
			super();
			this.element = element;
		}
		public Node() {
			super();
		}
		
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		
		public Node<E> getPrevious(){
			return this.previous;
		}
		
		public void setPrevious(Node<E> previous) {
			this.previous = previous;
		}

		
	}
	
	private Node<E> header;
	private int currentSize;
	Comparator<E> cmp;
	
	public CircularSortedDoublyLinkedList(Comparator<E> cmp) {
		this.header = new Node<>();
		this.currentSize = 0;
		this.cmp = cmp;
		this.header.setNext(header);
		this.header.setPrevious(header);
	}
	
	public CircularSortedDoublyLinkedList() {
		this.header = new Node<>();
		this.currentSize = 0;
		this.header.setNext(header);
		this.header.setPrevious(header);
	}
	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return new CircularSortedDoublyLinkedListIterator<E>();
		
	}

	@Override
	public boolean add(E obj) {
		//Creating the node that will be added.
		Node<E> newNode = new Node<E>(obj);
		
		//Caso Base: Cuando la lsita esta vacia, lo a~ade en la primera posicion
		if(this.isEmpty()) {
			this.header.setNext(newNode);
			this.header.setPrevious(newNode);
			
			newNode.setNext(this.header);
			newNode.setPrevious(this.header);
			this.currentSize++;
			return true;
		}
		else {
			//SI no esta vacia, va elemento por elemento buscando a ver si alguno de los elementos
			//es mayor que el new obj a a~adirse. Si encuentra alguno, a~ade el obj antes de el lemento de la lista que es mayor
			for(Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext() ) {
				if(cmp.compare(newNode.getElement(), temp.getElement()) <= 0) {
					temp.getPrevious().setNext(newNode);
					newNode.setPrevious(temp.getPrevious());
					newNode.setNext(temp);
					temp.setPrevious(newNode);
					
					this.currentSize++;
					return true;
					
				}
			}
				//Si sale del for loop, significa que no encontro ningun elemento que sea mayor, asi que el nuevo obj iria al final
				this.header.getPrevious().setNext(newNode);
				newNode.setPrevious(this.header.getPrevious());
				newNode.setNext(this.header);
				this.header.setPrevious(newNode);
				this.currentSize++;
				return true;	
			
		}
		
	}
	//Devuelve el size de la lista
	@Override
	public int size() {
		return this.currentSize;
	}
	
	@Override
	public boolean remove(E obj) {
		//Si la lista esta empty, devuelve falso
		if(this.isEmpty()) {
			return false;
		}
		else {
			//Si no esta empty, cambia el nex del nodo anterior, el previous del nodo siguiente
			//y conivierte el next y el previous del nodo a removerse a nulo (igual que su elemento)
			for(Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext()) {
				if(this.cmp.compare(temp.getElement(), obj) == 0) {
					temp.getPrevious().setNext(temp.getNext());
					temp.getNext().setPrevious(temp.getPrevious());
					temp.setElement(null);
					temp.setNext(null);
					temp.setPrevious(null);
					this.currentSize--;
					return true;
					 
				}
			}
		}
		return false;
	}
	//remueve unn node en un indice indicado
	@Override
	public boolean remove(int index) {
		if(this.isEmpty() || index >= this.currentSize || index < 0) {
			throw new IndexOutOfBoundsException();
	
		}
		else { 
			int i = 0;
			//Si no esta empty, cambia el nex del nodo anterior, el previous del nodo siguiente
			//y conivierte el next y el previous del nodo a removerse a nulo (igual que su elemento)
			for(Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext()) {
				if(i == index) {
					temp.getPrevious().setNext(temp.getNext());
					temp.getNext().setPrevious(temp.getPrevious());
					temp.setNext(null);
					temp.setPrevious(null);
					temp.setElement(null);
					this.currentSize--;
					return true;
				}
				i++;
			}
		}
		return false;
	}

	@Override
	public int removeAll(E obj) {
		
		if(!this.contains(obj)) {
			//Si no contiene el obj devuelve 0
			return 0;
		}
		//Utilizo el remove(object) para remover todos los elementos obj de la lista.
		else {
			int count = 0;
			while(this.remove(obj)) {
				count++;
			}
			return count;
		}
		 
	}
	//Devuelve el pimer elemento en la lista
	@Override
	public E first() {
		if(this.isEmpty()) {
			return null;
		}
		else {
			return this.header.getNext().getElement();
		}
		
	}
	//Devuelve el elemento previous al header (Si la lista esta vacia, devuelve nulo)
	@Override
	public E last() {
		if(this.isEmpty()) {
			return null;
		}
		else {
			return this.header.getPrevious().getElement();
		}
		
	}
	//Devuelve el eemento en el indece index
	@Override
	public E get(int index) {
		//Si el indice es menor que 0 o mayor/igual al current size, el indece esta out of bound
		if ((index < 0) || index >= this.currentSize) {
			throw new IndexOutOfBoundsException();
		}
		else {
			
			int currentPosition=0;
			Node<E> temp = this.header.getNext();
			//El while se detiene cuando estoy en el indece/nodo indicado
			while(currentPosition != index) {
				temp = temp.getNext();
				currentPosition++;
			}
			return temp.getElement();
		}
		
		
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
		
	}
	//Devuelve cierto si contiene el elemento e, si no, pues falso
	@Override
	public boolean contains(E e) {
		if(this.isEmpty()) {
			return false;
		}
		else {
			//Verifica cada elemento de la lista a ver si es igual a e
			for(Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext()) {
				if(temp.getElement().equals(e)) {
					return true;
				}
			}
		}
		//Si sale del for loop, significa que no lo consiguio, asi que devuelve falso
		return false;
	}

	@Override
	public boolean isEmpty() {
		
		return this.header.getNext().equals(this.header);
	}
	//Devuelve el index donde aparece el elemento e por primera vez
	@Override
	public int firstIndex(E e) {
		if(this.isEmpty()) {
			return -1;
		}
		else {	
			int i = 0;
			for (Node<E> temp = this.header.getNext(); !temp.equals(this.header); 
					temp = temp.getNext(), ++i) {
				if (temp.getElement().equals(e)) {
					return i;
				}
			}
		}
		// not found
		return -1;
	}
	//Devuelve el index donde aparece el elemento e por ultima vez
	@Override
	public int lastIndex(E e) {
		if(this.isEmpty()) {
			return -1;
			
		}
		else {
			int index = this.currentSize - 1;
			//Comienzo de atras para adelante, haciendo el temp cada vez temp.getPrevious()
			for(Node<E> temp = this.header.getPrevious(); temp != this.header; temp = temp.getPrevious()) {
				if(temp.getElement().equals(e)){
					return index;
				}
				index--;
				
			}
			return index;
		}
		

	}
		

}
