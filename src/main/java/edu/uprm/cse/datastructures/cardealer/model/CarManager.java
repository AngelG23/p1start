package edu.uprm.cse.datastructures.cardealer.model;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA.MapEntry;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {
	private final HashTableOA<Integer, Car> cList = CarList.getInstance();
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public SortedList<Car> getAllCar() {
//		Car[] result = new Car[cList.size()];
//		for(int i = 0; i< result.length; i++) {
//			result[i] = cList.get(i);
//		}
		return cList.getValues();
		
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getID(@PathParam("id") int id) {
//		Car c = null;
//		for(int i = 0; i < cList.size(); i++) {
//			if(cList.get(i).getCarId() == id) {
//				c = cList.get(i);
//			}
//		}
		Car c = cList.get(id);
		if(c != null) {
			return c;
		}
		else {
			 throw new NotFoundException();
		}
		
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car c) {
		MapEntry<Integer, Car> added = new MapEntry<Integer, Car>((int) c.getCarId(),
				c, false);
		if(cList.contains(added.getKey())) {
			throw new NotFoundException();
		}
		cList.put((int) c.getCarId(), c);
		return Response.status(201).build();
	}
	
	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response carDelete(@PathParam("id") int id) {
		if(cList.contains(id)) {
			cList.remove(id);
			return Response.status(201).build();
		}
				
				
		else {
			throw new NotFoundException();
		}
		
	}
		
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(Car c) {

		MapEntry<Integer, Car> removed = new MapEntry<Integer, Car>((int) c.getCarId(),
				c, false);
		if(cList.contains((int) c.getCarId())) {
			cList.remove((int) c.getCarId());
			cList.put((int) c.getCarId(), c);
			return Response.status(200).build();
		}else {
			throw new  NotFoundException();
		}
				
				
			
		
		
		
		
	}
}
